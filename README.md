# PowerGrid
### How to Build
You will need CMake to build the project
1. Create a build folder at the root of the project 
2. cd into the build directory
3. Run `cmake ..` or `cmake -G [Your Generator] ..` 

For the list of generator, run `cmake --help`