//
// Created by Nicolas Prudhomme on 2019-03-27.
//

#include <iostream>
#include <vector>
#include <set>
#include "Auctioneer.h"
#include "UserPrompt.h"

using std::pair;

void Auctioneer::auctionPhase(bool isFirstTurn)
{
    // First: pointer to a player, Second: If they have purchased a power plant
    std::vector< pair< Player*, bool >> playersEligibleToPurchase;
    for(auto& player : *(activePlayersPointer)){
        playersEligibleToPurchase.emplace_back(&player, false);
    }
    std::cout << "==== Time for some Power Plant Auctions 💸 =====" << std::endl;
    // Main loop to go through all players eligible to purchase one by one
    for(int i = 0; i < playersEligibleToPurchase.size(); ++i) {
        // Since all players need to buy a PowerPlant on first turn, everyone needs to make an offer.
        // Ask only if this not first turn.
        if(!isFirstTurn) {
            // If the current player doesn't or can't make an offer continue to the next player
            if(!wantsToAuction(playersEligibleToPurchase[i])){
                playersEligibleToPurchase[i].second = true; //Eliminate the player from the next rounds of auction
                continue;
            }
        }

        // Goes to this branch if a player wants to auction
        std::vector<Player*> playersForNextAuctionTour;

        //Put the players eligible for purchase with the player who initialized the auction first.
        int k = i;
        for(int j = 0; j < playersEligibleToPurchase.size(); ++j){
            // if the player has not purchase yet
            if(!playersEligibleToPurchase[k].second){
                playersForNextAuctionTour.push_back(playersEligibleToPurchase[k].first);
            }
            k = static_cast<int>((k + 1) % playersEligibleToPurchase.size());
        }

        //Auction one power plant
        auto winner = auctionOnePowerPlant(playersForNextAuctionTour);

        //Flag the winning player out for next round
        flagWinner(winner, playersEligibleToPurchase);
    }
}

Player* Auctioneer::auctionOnePowerPlant(const std::vector<Player*>& playersInCurrentAuction)
{
    // Player who initialize the Auction should go first
    PowerPlant* powerPlantOnAuction = selectPowerPlantToAuction(playersInCurrentAuction.front());
    // Player 1 then make an offer with a number greater than the PowerPlant number;
    std::cout << "Auctioning on: " << powerPlantOnAuction->to_string() << std::endl;
    std::cout << "-- Setting the initial bid --" << std::endl;
    int initialBid = getBid(powerPlantOnAuction->getNumber(), true, playersInCurrentAuction.front());
    // Get the winner from the round
    pair<Player*, int> winner = roundOfOffer(
            playersInCurrentAuction, initialBid, playersInCurrentAuction[0], true);
    //Make the transaction
    winner.first->takeElektron(winner.second);
    winner.first->acquirePowerPlant(*powerPlantOnAuction);
    deckManagerPointer->removeFromMarket(powerPlantOnAuction);
    //Announce the winner
    std::cout << "PowerPlant goes to: " << *winner.first << ", For " << winner.second << " Elektrons." << std::endl;
    std::cout << "Congradulations! 🎉\n" << std::endl;

    //Player Trace
    winner.first->printPossession();
    return winner.first;
}

/// TESTED
pair<Player*, int> Auctioneer::roundOfOffer(const std::vector<Player*>& players, int currentBid,
        Player* currentHighestBidder, bool firstRound)
{
    int highestBid = currentBid;
    Player* highestBidder = currentHighestBidder;
    int numberOfPass = 0;
    // First: pointer to player. Second: current bid.
    std::vector< pair<Player*, int> > playersCurrentBid;
    // Setting up the vector
    auto playersIt = players.begin();
    //if the first round take the current (initial bid) with first player and push back first
    if(firstRound){
        playersCurrentBid.emplace_back( pair<Player*, int>(players.front(), currentBid));
        playersIt++;
    }
    //give everyone (else) the value of zero
    for(; playersIt != players.end(); ++playersIt){
        playersCurrentBid.emplace_back( pair<Player*, int>(*playersIt, 0) );
    }

    auto roundIt = playersCurrentBid.begin();
    if(firstRound)
        ++roundIt;
    for(; roundIt != playersCurrentBid.end(); ++roundIt){
        //Ask only if not the highest bidder
        if(roundIt->first != highestBidder){
            roundIt->second = getBid(highestBid, false, roundIt->first);
            if(roundIt->second == 0){
                ++numberOfPass;
            }
            // Must necessarily be higher than previous
            else {
                highestBidder = roundIt->first;
                highestBid = roundIt->second;
            }
        }
    }
    //If two players made an offer go to the next round
    if(numberOfPass < players.size() - 1){
        return roundOfOffer(players, highestBid, highestBidder, false);
    }
    else {
        //Return the player who won the bid, with the value of the bid.
        return {highestBidder, highestBid};
    }
}

/// TESTED
int Auctioneer::getBid(int minimumBid, bool initialBid, Player *player)
{
    std::string topPrompt = player->getPlayerName() + ", make an offer. This offer must be greater ";
    if(!initialBid) {
        topPrompt += "than " + std::to_string(minimumBid) + "\nYou can skip this round by entering 0.";
    }
    else {
        topPrompt+= "or equal than " + std::to_string(minimumBid);
    }

    UserPrompt userPrompt(topPrompt, "What is your offer?");
    bool isValid = false;
    int bid = 0;
    while(!isValid){
        bid = userPrompt.getValueFromUser();
        if(bid == 0 && !initialBid){
            std::cout << "You opted out of this round of offer." << std::endl;
            return bid;
        }
        else if((bid <= minimumBid && !initialBid) || (bid < minimumBid && initialBid)) {
            std::cout << "Your bid is too low!" << std::endl;
        }
        else if(bid > player->getElektron()) {
            std::cout << "You cannot bid more than you have Elektrons!" << std::endl;
        }
        else {
            isValid = true;
        }
    }
    return bid;
}

/// TESTED
bool Auctioneer::wantsToAuction(const std::pair< Player*, bool >& player)
{
    //If the player already bought a PowerPlant, he cannot start an auction
    if(player.second) //true = already bought a powerplant
        return false;
    std::string topPrompt = player.first->getPlayerName() + ", Do you want to start an auction?";
    UserPrompt userPrompt(topPrompt);
    int choice = userPrompt.getUserAction(std::vector< std::string >{"Auction", "Pass"});
    return !((bool) choice); //Auction -> 0, !Auction = true
}

/// TESTED
PowerPlant* Auctioneer::selectPowerPlantToAuction(Player *player)
{
    std::vector<PowerPlant*> actualMarket = deckManagerPointer->getActualMarket();
    std::string topPrompt = player->getPlayerName() + ", Select a Power Plant For Auction";
    UserPrompt userPrompt(topPrompt, "What PowerPlant would you like to auction");
    int choice = userPrompt.getUserAction(actualMarket);
    return actualMarket[choice];
}

void Auctioneer::flagWinner(Player *winner, std::vector<std::pair<Player *, bool>> &playersEligibleToPurchase)
{
    for(auto it : playersEligibleToPurchase)
    {
        if(*it.first == *winner){
            it.second = true;
            return;
        }
    }
}


std::vector<Player> *Auctioneer::getActivePlayersPointer() const
{
    return activePlayersPointer;
}


