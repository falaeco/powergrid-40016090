//
// Created by Nicolas Prudhomme on 2019-03-27.
//

#ifndef POWERGRID_AUCTIONEER_H
#define POWERGRID_AUCTIONEER_H

#include <utility>
#include "Player.h"
#include "DeckManager.h"
#include "PowerPlant.h"

/**
 * Make the transactions for the players and transfer powerplants.
 * Candidate for strategy
 */
class Auctioneer
{
    /// Pointers to players and deck?
    DeckManager* deckManagerPointer;
    std::vector<Player>* activePlayersPointer;

public:
    Auctioneer()
    : deckManagerPointer(nullptr), activePlayersPointer(nullptr) {};
    Auctioneer(DeckManager* deckManager, std::vector<Player>* activePlayers)
    : deckManagerPointer(deckManager), activePlayersPointer(activePlayers) {};

    std::vector<Player> *getActivePlayersPointer() const;

//TODO: Most of these can be private

    /**
     * Query a player if he wants to start an auction
     * @param player
     *      First: pointer to player,
     *      Second: If the player has purchased a power plant
     * @return True if player wants to auction, false otherwise
     */
    bool wantsToAuction(const std::pair< Player*, bool >& player);

    /**
     * One player select a PowerPlant to put on auction
     * @param player that initiated the auction
     * @return the PowerPlant to put on auction
     */
    PowerPlant* selectPowerPlantToAuction(Player *player);

    /**
     * Get a bid from one player. The player can also skip a bid by entering the
     * number 0.
     * @param minimumBid the minimum bid to be a valid bid on the auction
     * @param initialBid sets to true if the bid cannot be skipped (typically
     * when a player initiate an auction)
     * @param player a pointer to the player currently making the bid
     * @return the amount of the bid or 0 if the player decides to skip.
     */
    int getBid(int minimumBid, bool initialBid, Player *player);

    /**
     * Recursive function to get the highest bid for one powerplant auction. The
     * recursion stop after the size of players - 1 pass on offering more.
     * @param players a vector of players eligible to auction on the current
     * powerplant, that is all players who have not yet purchased a powerplant
     * @param currentBid The current highest bid for the auction. Starts with
     * the initial bid of the player who initiate the auction
     * @param currentHighestBidder The current highest bidder for the auction.
     * Start with the player who initiate the auction.
     * @param firstRound Passing a value of true skips the first player, which
     * should be the player who initiate the auction.
     * @return The winning player with the amount of the winning bid.
     */
    std::pair<Player*, int> roundOfOffer(
            const std::vector<Player*>& players, int currentBid,
            Player* currentHighestBidder, bool firstRound);

    /**
     * Flag the winner of an auction and change its flag to true so the player
     * cannot participate to other auctions
     * @param winner The player who won
     * @param playersEligibleToPurchase vector of players coming from the auction
     * phase.
     */
    void flagWinner(Player* winner, std::vector< std::pair<Player*, bool>>& playersEligibleToPurchase);

    /**
     * Process of auctioning one power plant. This process consist of selecting
     * one PowerPlant to auction, making an inital offer, and doing rounds of
     * offers until there is one winner.
     * @param playersInCurrentAuction
     * @return
     */
    Player* auctionOnePowerPlant(const std::vector<Player*>& playersInCurrentAuction);

    /**
     * Operate a PowerPlant auction phase.
     * @param players the vector of players from the gameboard
     * @param deckManager the deckManager from the gameboard
     * @param isFirstTurn if this is the first turn, all players needs to acquire
     * a powerplant.
     */
    void auctionPhase(bool isFirstTurn);
};


#endif //POWERGRID_AUCTIONEER_H
