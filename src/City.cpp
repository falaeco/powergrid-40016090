//
// Created by Nicolas Prudhomme on 2019-02-24.
//

#include "City.h"

std::string City::getCityName() const {
    return city_name;
}

void City::addNeighbour(City & node, int cost) {
    neighbours.emplace_back(std::pair<City*, int>(&node, cost));
}

const std::vector<std::pair<City *, int>> City::getNeighbours() const {
    return neighbours;
}


bool operator==(const City &lhs, const City &rhs) {
    return lhs.getCityName() == rhs.getCityName();
}
