
//
// Created by Nicolas Prudhomme on 2019-02-24.
//

#ifndef POWERGRID_CITYNODE_H
#define POWERGRID_CITYNODE_H


#include <string>
#include <vector>
#include <utility>
#include "Pieces.h"

class City {
private:
    std::string city_name;
    std::vector<House> houses;
    std::vector<std::pair<City*, int>> neighbours;
public:
    City() = default;
    City(std::string city_name) : city_name(std::move(city_name)) {};

    std::string getCityName() const;
    const std::vector<std::pair<City*, int>> getNeighbours() const;

    void addNeighbour(City & node, int cost);
    
};

struct NodeHasher{
    size_t operator()(const City& node) const{
        return std::hash<std::string>()(node.getCityName());
    }
};

struct NodeComparator{
    bool operator()(const City& node1, const City& node2) const{
        return node1.getCityName() == node2.getCityName();
    }
};

bool operator==(const City& lhs, const City& rhs);


#endif //POWERGRID_CITYNODE_H
