//
// Created by Nicolas Prudhomme on 2019-03-03.
//

#include "DeckManager.h"
#include <json.hpp>
#include <array>
#include <fstream>
#include <random>
#include <iostream>

DeckManager::DeckManager() = default;

void DeckManager::loadDeck() {
    std::ifstream inputStream("../powerplants.json");
    nlohmann::json json;
    inputStream >> json;

    //Make the stack from Json
    auto stack = json["stack"];
    for(auto& powerplant : stack){
        deck.emplace_back(new PowerPlant(
                powerplant["powerPlantNumber"],
                powerplant["resourceType"].get<ResourceType>(),
                powerplant["resourceNumber"],
                powerplant["poweredCities"]
                ));
    }
    inputStream.close();

    //TODO: Change this to fit an array.    
    std::move( deck.begin(), deck.begin() + 4, std::back_inserter(actualMarket));
    for(int i = 0; i < 4; i++){
        deck.pop_front();
    }

    std::move( deck.begin(), deck.begin() + 4, std::back_inserter(futureMarket));
    for(int i = 0; i < 4; i++){
        deck.pop_front();
    }
}

void DeckManager::shuffleDeck() {

    std::vector<PowerPlant*> auxiliary_deck;
    PowerPlant* card13 = nullptr;
    for(auto& it : deck){
        //if you find the card #13 keep a reference to it
        if(it->getNumber() == 13){
            card13 = it;
        }
        else {
            //push it to the auxiliary deck
            auxiliary_deck.push_back(it);
        }
    }

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist(0, static_cast<int>(auxiliary_deck.size() - 1));

    for(int i = 0; i < auxiliary_deck.size(); ++i){
        int rand1 = dist(gen); //generate a random number with the mt199937 engine between 0 and size - 1
        int rand2 = dist(gen);
        std::swap(auxiliary_deck[rand1], auxiliary_deck[rand2]); //swap position between the two
    }

    deck.assign(auxiliary_deck.begin(), auxiliary_deck.end()); //put it back to the deck
    if(card13 != nullptr){
        //if found, put back card #13 at the front
        deck.push_front(card13);
    }
}

void DeckManager::initializeDeck()
{
    loadDeck();
    shuffleDeck();
}

const std::vector<PowerPlant *> &DeckManager::getActualMarket() const
{
    return actualMarket;
}

void DeckManager::printDeckContent() const
{
    showActualAndFutureMarket();

    std::cout << "--- Content of the deck ---" << std::endl;
    for(auto& card : deck){
        card->printContent();
    }
}

void DeckManager::removeFromMarket(PowerPlant* powerPlant)
{
    bool found = false;
    //find the powerplant
    for (auto wrapIter = actualMarket.begin(); wrapIter != actualMarket.end(); ++wrapIter) {
        if(**wrapIter == *powerPlant){
            actualMarket.erase(wrapIter);
            found = true;
        }
    }
    if(!found){
        std::cout << "The PowerPlant you want to remove from actual market could not be found." << std::endl;
        std::cout << "The Market will remain untouched." << std::endl;
        return;
    }

    refreshMarket();
}

void DeckManager::refreshMarket()
{
    if(step3){
        return;
    }
    //Put one more in the future market
    futureMarket.push_back(deck.front());
    deck.pop_front();
    //Sort it
    std::sort(futureMarket.begin(), futureMarket.end(),
        //Lambda function: Dereference then sort
        [](PowerPlant* a, PowerPlant* b){
            return *a < *b;
        }
    );

    //Push lowest to the actual market
    actualMarket.push_back(futureMarket.front());
    futureMarket.pop_front();
}

void DeckManager::showActualAndFutureMarket() const
{
    std::cout << "---- In Actual Market -----" << std::endl;
    for(auto& card : actualMarket){
        card->printContent();
    }

    std::cout << "---- In Future Market -----" << std::endl;
    for(auto& card : futureMarket){
        card->printContent();
    }
}

