//
// Created by Nicolas Prudhomme on 2019-03-03.
//

#ifndef POWERGRID_MARKETMANAGER_H
#define POWERGRID_MARKETMANAGER_H

#include "PowerPlant.h"
#include <vector>
#include <array>
#include <deque>

/**
 * The DeckManager take care of updating actual and future market and signal
 * step 3 when the deck gets empty.
 */
class DeckManager {
private:
    std::deque<PowerPlant*> deck;
    std::vector<PowerPlant*> actualMarket;
    std::deque<PowerPlant*> futureMarket;
    bool step3 = false;

    /**
     * Draw one card from the deck and rebalance actual and future market.
     * Is private to prevent an overflow of the actual market (more than 4 power
     * plant in the actual market)
     */
    void refreshMarket();
public:
    DeckManager();

    const std::vector<PowerPlant *> &getActualMarket() const;

    void loadDeck();
    void shuffleDeck();
    void initializeDeck();

    void removeFromMarket(PowerPlant* powerPlant);

    void printDeckContent() const;
    void showActualAndFutureMarket() const;
};


#endif //POWERGRID_MARKETMANAGER_H
