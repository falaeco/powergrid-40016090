//
// Created by Nicolas Prudhomme on 2019-03-03.
//

#ifndef POWERGRID_ENUMS_H
#define POWERGRID_ENUMS_H

#include <json.hpp>

enum class Color{ BLUE, RED, YELLOW, GREEN, PURPLE, BROWN };
enum class ResourceType{ COAL, OIL, GARBAGE, URANIUM, HYBRID, ECO };

NLOHMANN_JSON_SERIALIZE_ENUM(Color, {
    {Color::BLUE,  "Blue" },  //0
    {Color::RED,   "Red" },   //1
    {Color::YELLOW,"Yellow" },//2
    {Color::GREEN, "Green" }, //3
    {Color::PURPLE,"Purple" },//4
    {Color::BROWN, "Brown"}   //6
});

NLOHMANN_JSON_SERIALIZE_ENUM(ResourceType , {
    {ResourceType::COAL,    "Coal" }, //0
    {ResourceType::OIL,     "Oil" },  //1
    {ResourceType::GARBAGE, "Garbage" }, //2
    {ResourceType::URANIUM, "Uranium" }, //3
    {ResourceType::HYBRID,  "Hybrid"},//4
    {ResourceType::ECO,     "Eco"}    //5
});

namespace EHelper {
    inline std::string Color_to_string(Color color)
    {
        switch (color){
            case Color::BLUE:    return "Blue";
            case Color::RED:     return "Red";
            case Color::YELLOW:  return "Yellow";
            case Color::GREEN:   return "Green";
            case Color::PURPLE:  return "Purple";
            case Color::BROWN:   return "Brown";
        }
    }

    inline Color Color_from_string(const std::string& string)
    {
        if(string == "Red") return Color::RED;
        if(string == "Yellow") return Color::YELLOW;
        if(string == "Green") return Color::GREEN;
        if(string == "Purple") return Color::PURPLE;
        if(string == "Brown") return Color::BROWN;
        return Color::BLUE; //Return the first one if none or blue is found
    }

    inline std::string RType_to_string(ResourceType resourceType)
    {
        switch (resourceType)
        {
            case ResourceType::COAL:    return "Coal";
            case ResourceType::OIL:     return "Oil" ;
            case ResourceType::GARBAGE: return "Garbage";
            case ResourceType::URANIUM: return "Uranium";
            case ResourceType::HYBRID:  return "Hybrid";
            case ResourceType::ECO:     return "Eco";
        }
    }
}

#endif //POWERGRID_ENUMS_H
