//
// Created by Nicolas Prudhomme on 2019-03-27.
//
#include <set>
#include <algorithm>

#include "GameBoard.h"
#include "UserPrompt.h"

void GameBoard::startGame()
{
    //Select a map
    mapLoader.initializeMap();

    //Set Up Players
    setUpPlayers();

    //Prepare Game
    deckManager.initializeDeck();
    resourceManager.initializeResourceMarket();

    printStats();
}

/// Setting this up in external class?
void GameBoard::setUpPlayers()
{
    //Select the number of players
    UserPrompt userPrompt("Select the number of player: ", "How many players");
    numberOfPlayers = userPrompt.getUserAction(
            std::vector<std::string>{"2 players", "3 players", "4 players", "5 players", "6 players"});
    numberOfPlayers += 2; //Index 0 = 2 players
    players.reserve(numberOfPlayers);

    //Select Area
    //TODO: Implement area
    std::set<std::string> regionSelection = {
            EHelper::Color_to_string(Color::BLUE),
            EHelper::Color_to_string(Color::RED),
            EHelper::Color_to_string(Color::YELLOW),
            EHelper::Color_to_string(Color::GREEN),
            EHelper::Color_to_string(Color::PURPLE),
            EHelper::Color_to_string(Color::BROWN),
    };

    std::vector<std::string> regionChoices;
    userPrompt.setBottomPrompt("Select A Region");
    for(int i = 1; i <= numberOfPlayers; ++i) {
        regionChoices.assign(regionSelection.begin(), regionSelection.end());
        std::string topPrompt = "Player " + std::to_string(i) + ", Select a Region";
        userPrompt.setTopPrompt(topPrompt);

        int selectedRegionIndex = userPrompt.getUserAction(regionChoices);
        std::string selectedRegion = regionChoices[selectedRegionIndex];
        // Create a player with the selected color
        players.emplace_back(
                Player(EHelper::Color_from_string(selectedRegion)));
        // Remove the color from the set
        regionSelection.erase(selectedRegion);
    }
}

void GameBoard::printStats()
{
    std::cout << "Number of Players: " << players.size() << std::endl;
    std::cout << "============ Possession of the players: ===============" << std::endl;
    for(auto& player : players) {
        player.printPossession();
    }
    std::cout << "============ End of possession of players =============" << std::endl;
    std::cout << "============ In the Deck: =============================" << std::endl;
    deckManager.printDeckContent();
    std::cout << "============ End Of Deck: =============================" << std::endl;
    std::cout << "============ In the Market: =============================" << std::endl;
    resourceManager.printMarketContent();
    std::cout << "============ End of Market =============================" << std::endl;
}

void GameBoard::auctionPhase()
{
    reorderPlayers();

    // Select a PowerPlant to auction.
}

void GameBoard::reorderPlayers()
{
    std::sort(players.begin(), players.end());
}

void GameBoard::reverseOrder()
{
    std::reverse(players.begin(), players.end());
}
