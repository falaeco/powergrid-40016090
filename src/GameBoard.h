//
// Created by Nicolas Prudhomme on 2019-03-27.
//

#ifndef POWERGRID_GAMEBOARD_H
#define POWERGRID_GAMEBOARD_H

#include "Map.h"
#include "MapLoader.h"
#include "Player.h"
#include "DeckManager.h"
#include "ResourceManager.h"

/**
 * Contains all the different elements of the game and controls the flow of the
 * game using the different sub-component class.
 */
class GameBoard
{
    Map* map = nullptr;
    MapLoader mapLoader;
    DeckManager deckManager;
    ResourceManager resourceManager;

    int numberOfPlayers;
    std::vector<Player> players;

    int numberOfTurns = 0;

    void setUpPlayers();

public:
    GameBoard() = default;

    void startGame();
    void printStats();
    void reorderPlayers();
    void reverseOrder();
    void auctionPhase();

};


#endif //POWERGRID_GAMEBOARD_H
