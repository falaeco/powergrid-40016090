//
// Created by Nicolas Prudhomme on 2019-02-24.
//

#include <unordered_set>
#include <queue>
#include "Map.h"

Map::Map(uint size) {
    vertex_list.reserve(size);
}

// Deleting these nodes may cause dangling pointers in the players...
Map::~Map() {
    for(auto& it : vertex_list){
        delete it;
        it = nullptr;
    }
    vertex_list.clear();
}

uint Map::size() const {
    return vertex_list.size();
}

City & Map::vertexAt(uint index) const {
    return *vertex_list[index];
}

const std::vector<City *> &Map::getVertex_list() const {
    return vertex_list;
}


void Map::addVertex(const std::string& city_name) {
    vertex_list.emplace_back(new City(city_name));
}

void Map::addEdge(int src, int dest, int cost) {
    // Check if the edge already exists
    for(auto it : vertex_list[src]->getNeighbours()){
        //if the element points to the same address return
        if(it.first == vertex_list[dest]){
            std::cout << "This edge already exist" << std::endl;
            return;
        }
    }
    vertex_list[src]->addNeighbour(*vertex_list[dest], cost);
    vertex_list[dest]->addNeighbour(*vertex_list[src], cost);
}

/**
 * Print the graph with connecting nodes underlaying
 */
void Map::printGraph() {
    std::cout << "Graph contains: \n";
    for(auto& it : vertex_list){
        std::cout << it->getCityName() << "\n";
        std::cout << "\t Connects to: ";
        auto itNeighbours = it->getNeighbours();
        // Does not print the neighbours
        for(auto it2 : itNeighbours){
            std::cout << it2.first->getCityName() << ", ";
        }
        std::cout << "\n";
    }
}

/**
 * Breadth-First search traversal to determine if all nodes are connected through
 * a path.
 * @return true if all the nodes of the graph has been visited, false otherwise
 */
bool Map::isConnected() const {
    std::unordered_set<City, NodeHasher, NodeComparator> visitedSet;
    std::queue<City> discoveryList;

    // Find the first node and find the first node who has neighbours in discovery list.
    for(auto& it : getVertex_list()){
        if(!it->getNeighbours().empty()){
            discoveryList.push(*it);
            break;
        }
    }

    // Iterate through the nodes as long as the discovery list is empty
    while(!discoveryList.empty()){
        City visiting = discoveryList.front();
        //if currently visiting is not part of the set
        if(visitedSet.find(visiting) == visitedSet.end()) {
            //put the node in the visited set
            visitedSet.insert(visiting);
            //put all the neighbours in the discoverylist;
            for(auto& it : visiting.getNeighbours()){
                discoveryList.push(*it.first);
            }
        }
        //repeat with the next element in the discovery list
        discoveryList.pop();
    }
    std::cout << "Size of visitedSet: " << visitedSet.size() << std::endl;
    return visitedSet.size() == size();
}

