//
// Created by Nicolas Prudhomme on 2019-02-24.
//

#ifndef POWERGRID_MAP_H
#define POWERGRID_MAP_H

#include <vector>
#include <iostream>
#include "City.h"

/**
 * Map stores all the vertices of the game and is the owner and responsible
 * destructor of those nodes. If the map is destroyed before the players, the
 * pointers of the region owned by a player are invalid...
 */
class Map {
private:
    std::vector<City*> vertex_list;

public:
    Map() = default;
    Map(uint size);
    ~Map();

    uint size() const;
    City & vertexAt(uint index) const;
    const std::vector<City *> &getVertex_list() const;

    void addVertex(const std::string& city_name);
    void addEdge(int src, int dest, int cost);
    void printGraph();
    bool isConnected() const;
};


#endif //POWERGRID_MAP_H
