//
// Created by Nicolas Prudhomme on 2019-03-01.
//

#include "MapLoader.h"
#include "UserPrompt.h"

namespace fsyst = boost::filesystem;

//TODO: Fraction this
Map* MapLoader::loadMap(const std::string &filename) {
    std::ifstream inputStream(filename);
    if(inputStream.fail()){
        inputStream.close();
        std::cout << "The file could not be found... Are you sure you are in the right directory?";
        return nullptr;
    }

    nlohmann::json json;
    //get the stream into the json object
    inputStream >> json;
    inputStream.close();
    Map* map = nullptr;
    try {
        auto nodes = json["nodes"];
        uint size = nodes.size();
        map = new Map(size);
        for (auto &node : nodes) {
            if(!node.is_string()){ //throw an error if a node in the list is not a string
                throw std::runtime_error("Expected a string but received a non-string type\n");
            }
            map->addVertex(node);
        }

        auto edges = json["connections"];
        int counter = 0;
        for (auto &edge : edges) {
            if(edge[0] >= size || edge[1] >= size){
                //throw an error if one of the edge is bigger than the current size of the map.
                std::cout << "Invalid edge at: " << counter << std::endl;
                throw std::runtime_error("Error while parsing edges\n");
            }
            map->addEdge(edge[0], edge[1], edge[2]);
            ++counter;
        }
    }
    catch (nlohmann::json::exception& e)
    {   //catch any json related formatting error
        std::cout << "File is not compliant to JSON Rules\n";
        std::cout << "Message: " << e.what() << '\n';
        std::cout << "Exception ID: " << e.id;
        return nullptr;
    }
    catch (std::runtime_error& e)
    {   //catch other errors thrown during runtime;
        //could be improved by implementing own runtime error class
        std::cout << "Runtime error: " << e.what();
        return nullptr;
    }
    return map;
}

std::vector<std::string> MapLoader::loadDirectory()
{
    std::vector<std::string> mapFiles;
    fsyst::path path(pathToMapFiles);
    for (auto& file : fsyst::directory_iterator(path)){
        mapFiles.push_back(file.path().stem().string());
    }

    return mapFiles;
}

std::string MapLoader::getUserSelection()
{
    std::vector<std::string> mapChoices = loadDirectory();
    UserPrompt userPrompt("Select your Map", "Which map you want to play on?");
    int userChoice = userPrompt.getUserAction(mapChoices);
    mapFile = "../map/" + mapChoices[userChoice] + ".map";
    return mapFile;
}

bool MapLoader::isValidMap(Map *map)
{
    if(!map)
        return false;
    return map->isConnected();
}

Map *MapLoader::initializeMap()
{
    Map* map = nullptr;
    while(true)
    {
        getUserSelection();
        map = loadMap(mapFile);
        if(!isValidMap(map)){
            std::cout << "The map you tried to load is not a valid map, choose another one";
        }
        else {
            return map;
        }
    }
}
