//
// Created by Nicolas Prudhomme on 2019-03-01.
//

#ifndef POWERGRID_MAPLOADER_H
#define POWERGRID_MAPLOADER_H

#include <string>
#include <fstream>
#include <json.hpp>
#include <boost/filesystem.hpp>

#include "Map.h"

/**
 * @class MapLoader
 *
 * MapLoader class that loads a map and return the address of the built map.
 * The class loader uses a json parser to easily access size and ensure that
 * all information is correctly formatted. The JSON format is also easily
 * expendable and flexible should we need to add more complex functionality.
 */

class MapLoader {
    std::string pathToMapFiles;
    std::string mapFile;
public:
    MapLoader() : pathToMapFiles("../map") {};

    /**
     * Read a map file and return a pointer to the map created.
     * @param filename The path to the file.
     * @return Pointer to the map created.
     */
    Map* loadMap(const std::string &filename);

    /**
     * Check if the map enter in the argument is valid
     * @param map The map to verify
     * @return validity.
     */
    bool isValidMap(Map *map);

    /**
     * Read all Map files from the directory saved in pathToMapFiles
     * @return The Name of each map file in the directory
     */
    std::vector<std::string> loadDirectory();

    /**
     * Query the user for a map to play
     * @param choices
     * @return the path to the file to play
     */
    std::string getUserSelection();

    /**
     * Prompt the user to select a map and loads a valid map.
     * @return
     */
    Map* initializeMap();
};


#endif //POWERGRID_MAPLOADER_H
