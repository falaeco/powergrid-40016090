//
// Created by Nicolas Prudhomme on 2019-04-01.
//

#ifndef POWERGRID_NODE_H
#define POWERGRID_NODE_H

#include <vector>
#include <string>

/**
 * Node data structure, used for Cities and Regions. Needs to be a template
 * because type between a City and a Region are too different. Or make it as
 * an abstract class?
 */
 template<class T>
class Node
{
    //Change this to a set?
    std::vector<T> neighbours;
public:
    virtual std::string getName() const;
    void addNeighbour;
};


#endif //POWERGRID_NODE_H
