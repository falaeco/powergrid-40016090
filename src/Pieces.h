//
// Created by Nicolas Prudhomme on 2019-02-24.
//

#ifndef POWERGRID_PIECES_H
#define POWERGRID_PIECES_H

#include <string>
#include "Enums.h"

class Pieces {

};

class House : public Pieces {
private:
    Color color_;
public:
    House() = default;
    House(Color color) : color_(color) {};
    Color color() const;
};


#endif //POWERGRID_PIECES_H
