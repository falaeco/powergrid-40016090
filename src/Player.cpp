//
// Created by Nicolas Prudhomme on 2019-02-24.
//

#include <iostream>
#include "Player.h"

Player::Player() {
    color = Color::BLUE;
    Elektron = 50;
    Houses = 22;
    resourceTokens = {
        {ResourceType::COAL, 0},
        {ResourceType::OIL, 0},
        {ResourceType::GARBAGE, 0},
        {ResourceType::URANIUM, 0}
    };
}

Player::Player(Color color) {
    this->color = color;
    Elektron = 50;
    Houses = 22;
    resourceTokens = {
            {ResourceType::COAL, 0},
            {ResourceType::OIL, 0},
            {ResourceType::GARBAGE, 0},
            {ResourceType::URANIUM, 0}
    };
}

Player::Player(Color color, int Elektron){
    this->color = color;
    this->Elektron = Elektron;
    Houses = 22;
    resourceTokens = {
            {ResourceType::COAL, 0},
            {ResourceType::OIL, 0},
            {ResourceType::GARBAGE, 0},
            {ResourceType::URANIUM, 0}
    };
}

Player::~Player() {
    //Is not responsible for deleting the nodes of his network.
    //Delete all the powerPlants the player acquired.
    for(auto& it : powerPlants){
        delete it;
        it = nullptr;
    }
    powerPlants.clear();
}

const std::string Player::getPlayerName() const {
    std::string colorName = EHelper::Color_to_string(color);
    return "Player " + colorName;
}

int Player::getElektron() const {
    return Elektron;
}

unsigned int Player::getNetworkSize() const {
    return network.size();
}

const std::vector<City *> &Player::getNetwork() const {
    return network;
}

const std::unordered_map<ResourceType, int> &Player::getResourceTokens() const {
    return resourceTokens;
}

const std::vector<PowerPlant *> &Player::getPowerPlants() const
{
    return powerPlants;
}

void Player::setElektron(const int amount) {
    Elektron = amount;
}

void Player::giveElektron(const int amount) {
    Elektron += amount;
}

void Player::takeElektron(const int amount) {
    Elektron -= amount;
}

void Player::addToNetwork(City& node) {
    network.emplace_back(&node);
}

void Player::acquirePowerPlant(PowerPlant &powerPlant) {
    powerPlants.emplace_back(&powerPlant);
}

void Player::acquireResource(ResourceType resourceType, int amount) {
    resourceTokens[resourceType] += amount;
}

void Player::printPossession() {
    std::cout << "~~~ " <<this->getPlayerName() << " Possession: ~~~" << std::endl;
    std::cout << this->getElektron() << " Elektrons" << std::endl;
    std::cout << "Ressources: \n";
    for(auto& resource : this->getResourceTokens()){
        std::cout << "\t" << EHelper::RType_to_string(resource.first) << " : " << resource.second << "\n";
    }
    //TODO: Change this for clearer lists
    std::cout << "\nPowerplants: " << std::endl;
    for(auto& plant : powerPlants){
        std::cout << *plant << std::endl;
    }
    std::cout << "\nNetwork: Size is " << getNetworkSize() << std::endl;
    for(auto& city : network){
        std::cout << city->getCityName() << ", ";
    }
    std::cout <<  "~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
}

std::ostream& operator<<(std::ostream& os, const Player& player)
{
    os << player.getPlayerName();
    return os;
}

bool Player::operator<(const Player &otherPlayer) const
{
    if(this->getNetworkSize() == otherPlayer.getNetworkSize()){
        int thisBiggestNumberedPowerPlant = 0;
        int otherBiggestNumberedPowerPlant = 0;
        for(auto& powerPlant : powerPlants){
            if(thisBiggestNumberedPowerPlant < powerPlant->getNumber()){
                thisBiggestNumberedPowerPlant = powerPlant->getNumber();
            }
        }
        for(auto& powerPlant : otherPlayer.getPowerPlants()){
            if(otherBiggestNumberedPowerPlant < powerPlant->getNumber()){
                otherBiggestNumberedPowerPlant = powerPlant->getNumber();
            }
        }
        return thisBiggestNumberedPowerPlant < otherBiggestNumberedPowerPlant;
    }
    return this->getNetworkSize() < otherPlayer.getNetworkSize();
}

bool Player::operator==(const Player &rhs) const
{
    return color == rhs.color &&
           Elektron == rhs.Elektron &&
           Houses == rhs.Houses &&
           resourceTokens == rhs.resourceTokens &&
           powerPlants == rhs.powerPlants &&
           network == rhs.network;
}

bool Player::operator!=(const Player &rhs) const
{
    return !(rhs == *this);
}
