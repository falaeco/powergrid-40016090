//
// Created by Nicolas Prudhomme on 2019-02-24.
//

#ifndef POWERGRID_PLAYER_H
#define POWERGRID_PLAYER_H

#include <vector>
#include <unordered_map>
#include <iostream>
#include "Pieces.h"
#include "PowerPlant.h"
#include "City.h"
/**
 * Model for the players
 * Players own elektrons, different resource tokens, and a vector of references
 * to the network they already own, which will be eventually helpful to return
 * the neighbours of the network for expansion.
 */
class Player {
private:
    Color color;
    int Elektron;
    int Houses;
    //TODO: Change to map?
    std::unordered_map<ResourceType, int> resourceTokens;
    std::vector<PowerPlant*> powerPlants;
    std::vector<City*> network;
public:
    Player();
    explicit Player(Color color);
    explicit Player(Color color, int Elektron);
    ~Player();

    const std::string getPlayerName() const;
    int getElektron() const;
    unsigned int getNetworkSize() const;
    const std::vector<PowerPlant *> &getPowerPlants() const;
    const std::vector<City*>& getNetwork() const;
    const std::unordered_map<ResourceType, int> &getResourceTokens() const;

    void setElektron(int amount);
    void giveElektron(int amount);
    void takeElektron(int amount);
    void addToNetwork(City& node);
    void acquirePowerPlant(PowerPlant& powerPlant);
    void acquireResource(ResourceType resourceType, int amount);

    void printPossession();

    /// Sort the players in the right order according to the rules.
    bool operator<(const Player& otherPlayer) const;
    friend std::ostream& operator<<(std::ostream& os, const Player& player);

    bool operator==(const Player &rhs) const;

    bool operator!=(const Player &rhs) const;
};


#endif //POWERGRID_PLAYER_H
