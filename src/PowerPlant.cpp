//
// Created by Nicolas Prudhomme on 2019-03-03.
//

#include "PowerPlant.h"
#include <iostream>

int PowerPlant::getNumber() const {
    return powerplant_number;
}

ResourceType PowerPlant::getResource_type() const {
    return resource_type;
}

int PowerPlant::getResource_number() const {
    return resource_number;
}

int PowerPlant::getCitiesPower() const {
    return cities_power;
}

void PowerPlant::printContent() const
{
    std::cout << this->getNumber() << " - ";
    std::cout << EHelper::RType_to_string(this->getResource_type()) << ": ";
    std::cout << this->getResource_number() << ", ";
    std::cout << "Powered Cities: " << this->getCitiesPower();
}

std::string PowerPlant::to_string() const
{
    return
        std::to_string(powerplant_number) + " - " +
        EHelper::RType_to_string(resource_type) + ": " + std::to_string(resource_number) +
        ", Powered Cities: " + std::to_string(cities_power);
}

std::ostream& operator<<(std::ostream& os, const PowerPlant& powerPlant){
    powerPlant.printContent();
    return os;
}

bool PowerPlant::operator<(const PowerPlant &otherPP) {
    return powerplant_number < otherPP.getNumber();
}

bool PowerPlant::operator==(const PowerPlant &rhs) const
{
    return powerplant_number == rhs.powerplant_number &&
           resource_type == rhs.resource_type &&
           resource_number == rhs.resource_number &&
           cities_power == rhs.cities_power;
}

bool PowerPlant::operator!=(const PowerPlant &rhs) const
{
    return !(rhs == *this);
}
