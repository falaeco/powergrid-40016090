//
// Created by Nicolas Prudhomme on 2019-03-03.
//

#ifndef POWERGRID_CARD_H
#define POWERGRID_CARD_H

#include <utility>
#include <iostream>
#include "Enums.h"
/**
 * Model for a PowerPlant Card with all its attributes.
 */
class PowerPlant {
private:
    int powerplant_number;
    ResourceType resource_type;
    int resource_number;
    int cities_power;
public:
    PowerPlant() = default;
    PowerPlant(int number, ResourceType type, int resource_number,int power) :
        powerplant_number(number), resource_type(type), resource_number(resource_number), cities_power(power) {};

    int getNumber() const;
    ResourceType getResource_type() const;
    int getResource_number() const;
    int getCitiesPower() const;

    std::string to_string() const;
    void printContent() const;

    friend std::ostream& operator<<(std::ostream& os, const PowerPlant& powerPlant);

    bool operator==(const PowerPlant &rhs) const;

    bool operator!=(const PowerPlant &rhs) const;

    bool operator <(const PowerPlant & otherPP);
};


#endif //POWERGRID_CARD_H
