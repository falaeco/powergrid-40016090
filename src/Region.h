//
// Created by Nicolas Prudhomme on 2019-04-01.
//

#ifndef POWERGRID_REGION_H
#define POWERGRID_REGION_H

#include <vector>
#include "Enums.h"
#include "City.h"
class Region
{
    Color regionColor;
    std::vector<City*> citiesInRegion;
    std::vector<Region*> neighbours;

public:

};


#endif //POWERGRID_REGION_H
