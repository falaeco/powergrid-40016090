//
// Created by Nicolas Prudhomme on 2019-03-27.
//

#include "ResourceManager.h"
#include <iostream>

ResourceManager::ResourceManager() = default;

void ResourceManager::initializeResourceMarket()
{
    regularMarket[0] = SlotTypeRegular(1, 3, 0, 0, 0);
    regularMarket[1] = SlotTypeRegular(2, 3, 0, 0, 0);
    regularMarket[2] = SlotTypeRegular(3, 3, 3, 0, 0);
    regularMarket[3] = SlotTypeRegular(4, 3, 3, 0, 0);
    regularMarket[4] = SlotTypeRegular(5, 3, 3, 0, 0);
    regularMarket[5] = SlotTypeRegular(6, 3, 3, 0, 0);
    regularMarket[6] = SlotTypeRegular(7, 3, 3, 3, 0);
    regularMarket[7] = SlotTypeRegular(8, 3, 3, 3, 0);
    uraniumMarket[0] = SlotTypeUranium(10, 0);
    uraniumMarket[1] = SlotTypeUranium(12, 0);
    uraniumMarket[2] = SlotTypeUranium(14, 1);
    uraniumMarket[3] = SlotTypeUranium(16, 1);
}

void ResourceManager::printMarketContent()
{
    std::cout << "---- Regular Market -----" << std::endl;
    for(auto& slot : regularMarket) {
        std::cout << "Slot Cost " << slot.cost << "\n\t";
        std::cout << slot.amountOfCoal << " Coal \n\t";
        std::cout << slot.amountOfOil << " Oil \n\t";
        std::cout << slot.amountOfUranium << " Uranium \n\t";
        std::cout << slot.amountOfGarbage << " Garbage \n";
    }

    std::cout << "---- Uranium Market -----" << std::endl;
    for(auto& slot : uraniumMarket) {
        std::cout << "Slot Cost " << slot.cost << "\n\t";
        std::cout << "Has Uranium: " << slot.hasUranium << "\n";
    }
}
