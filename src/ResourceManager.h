//
// Created by Nicolas Prudhomme on 2019-03-27.
//

#ifndef POWERGRID_RESOURCEMANAGER_H
#define POWERGRID_RESOURCEMANAGER_H

#include <array>
/**
 * Class Managing the Resource Market of the Game.
 */
class ResourceManager
{
    /**
     * Struct that can holds up to 3 coal, 3 oil, 1 uranium and 3 Garbage;
     */
    struct SlotTypeRegular
    {
        int cost;
        int amountOfCoal;
        int amountOfOil;
        int amountOfUranium;
        int amountOfGarbage;

        SlotTypeRegular() = default;
        SlotTypeRegular(int cost, int coal, int oil, int uranium, int garbage)
        : cost(cost), amountOfCoal(coal), amountOfOil(oil), amountOfUranium(uranium), amountOfGarbage(garbage) {};
    };

    /**
     * Struct holding only one uranium
     */
    struct SlotTypeUranium
    {
        int cost;
        bool hasUranium;
        SlotTypeUranium() = default;
        SlotTypeUranium(int cost, bool uranium)
        : cost(cost), hasUranium(uranium) {};
    };

    std::array<SlotTypeRegular, 8> regularMarket;
    std::array<SlotTypeUranium, 4> uraniumMarket;

public:
    ResourceManager();

    void initializeResourceMarket();
    void printMarketContent();
};


#endif //POWERGRID_RESOURCEMANAGER_H
