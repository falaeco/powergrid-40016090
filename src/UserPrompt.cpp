//
// Created by Nicolas Prudhomme on 2019-03-27.
//

#include "UserPrompt.h"
#include "PowerPlant.h"

template<class T>
int UserPrompt::getUserAction(const std::vector<T> &options)
{
    std::cout << topPrompt << std::endl;
    for(int i = 0; i < options.size(); ++i){
        std::cout << "[ " << i+1 << " ]: " << options[i] << std::endl;
    }
    std::cout << bottomPrompt << "\n ?-> ";
    int choice;
    std::cin >> choice;
    choice--; // Decrease the index because it starts from 1
    if(choice < 0 || choice >= options.size()){
        std::cout << "Your selection is out of range";
        return getUserAction(options);
    }
    else {
        return choice;
    }
}

int UserPrompt::getValueFromUser()
{
    std::cout << topPrompt << std::endl;
    std::cout << bottomPrompt << "\n ?-> ";
    int value;
    std::cin >> value;
    std::cout << std::endl;
    return value;
}

void UserPrompt::setTopPrompt(const std::string &topPrompt)
{
    UserPrompt::topPrompt = topPrompt;
}

void UserPrompt::setBottomPrompt(const std::string &bottomPrompt)
{
    UserPrompt::bottomPrompt = bottomPrompt;
}

template int UserPrompt::getUserAction<std::string>(const std::vector<std::string> &);
template<> int UserPrompt::getUserAction<PowerPlant*>(const std::vector<PowerPlant*> &options)
{
    std::vector< std::string > StringifiedOptions;
    for(auto& powerplant : options){
        StringifiedOptions.push_back(powerplant->to_string());
    }
    return getUserAction(StringifiedOptions);
}
