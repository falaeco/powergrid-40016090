#include <utility>

//
// Created by Nicolas Prudhomme on 2019-03-27.
//

#ifndef POWERGRID_USERPROMPT_H
#define POWERGRID_USERPROMPT_H

#include <string>
#include <iostream>
#include <vector>

/**
 * Class that interacts with the user to show options and get user input.
 */

class UserPrompt
{
    std::string topPrompt;
    std::string bottomPrompt;

public:
    UserPrompt() = default;
    explicit UserPrompt(std::string top)
    : topPrompt(top), bottomPrompt("") {};
    UserPrompt(std::string top, std::string bottom)
    : topPrompt(std::move(top)), bottomPrompt(std::move(bottom)) {};

    void setTopPrompt(const std::string &topPrompt);
    void setBottomPrompt(const std::string &bottomPrompt);

    /**
     * General template to get decisions from a player
     * @tparam T Type of options @attention: Type must have ostream operator overriden
     * @param options Options to show in the menu
     * @return the index of the option selected from the vector of options.
     */
    template <class T> int getUserAction(const std::vector<T>& options);

    int getValueFromUser();

    /// Auctioneer actions
};


#endif //POWERGRID_USERPROMPT_H
