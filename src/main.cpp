#include <iostream>
#include <vector>
#include <utility>

#include "Player.h"
#include "Enums.h"
#include "GameBoard.h"
#include "DeckManager.h"
#include "Auctioneer.h"
#include "PowerPlant.h"

using std::pair;

int main() {
    std::cout << "⚡️🏭⚡ Let's play Powergrid ⚡️🏭⚡" << std::endl;
    std::cout << "------------------------------------\n";

    // ===== AUCTIONEER DRIVER ======
    std::vector<Player> players = {
            Player(Color::BLUE, 200),
            Player(Color::GREEN, 200),
            Player(Color::YELLOW, 200),
            Player(Color::BROWN, 200)};

    std::vector<Player*> playerReferences{
        &players[0],
        &players[1],
        &players[2],
        &players[3],
    };

    DeckManager deckManager;
    deckManager.initializeDeck();

    Auctioneer auctioneer(&deckManager, &players);
    auctioneer.auctionPhase(true);
    // ===== END OF AUCTIONEER DRIVER =====

    return 0;
}
